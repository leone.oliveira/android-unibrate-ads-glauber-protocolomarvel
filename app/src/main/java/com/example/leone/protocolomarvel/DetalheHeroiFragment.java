package com.example.leone.protocolomarvel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leone.protocolomarvel.data.HeroiDAO;
import com.example.leone.protocolomarvel.model.Heroi;
import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso;

/**
 * Created by leone on 24/10/2015.
 */
public class DetalheHeroiFragment extends Fragment {

    Heroi mHeroi;
    MenuItem mMenuItemFavorito;
    HeroiDAO mDao;

    public static DetalheHeroiFragment novaInstancia(Heroi heroi){
        Bundle args = new Bundle();
        args.putSerializable("heroi", heroi);

        DetalheHeroiFragment dhf = new DetalheHeroiFragment();
        dhf.setArguments(args);
        return  dhf;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mDao = new HeroiDAO(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mHeroi = (Heroi)getArguments().getSerializable("heroi");
        View layout = inflater.inflate(R.layout.fragment_detalhe_heroi,container,false);

        TextView txtNome = (TextView)layout.findViewById(R.id.textView);
        TextView txtAno = (TextView)layout.findViewById(R.id.textView2);
        ImageView imgCapa = (ImageView)layout.findViewById(R.id.imgDetalheFragment);
        TextView txtRaca = (TextView)layout.findViewById(R.id.txtRaca);
        TextView txtNacionalidade = (TextView)layout.findViewById(R.id.txtNacionalidade);
        TextView txtStatus = (TextView)layout.findViewById(R.id.txtStatus);


        Picasso.with(getContext())
                .load(mHeroi.img)
                .into(imgCapa);

        txtNome.setText(mHeroi.nome);
        txtAno.setText(String.valueOf(mHeroi.ano_aparicao));
        txtRaca.setText(mHeroi.raca);
        txtNacionalidade.setText(mHeroi.nacionalidade);
        txtStatus.setText(mHeroi.status);


        return layout;


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_detalhe, menu);

        mMenuItemFavorito = menu.findItem(R.id.acao_favorito);
        atualizarMenu(mDao.isFavoritos(mHeroi));
    }

    private void atualizarMenu(boolean favorito){
        mMenuItemFavorito.setIcon(favorito ?
        android.R.drawable.ic_menu_delete :
        android.R.drawable.ic_menu_save);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.acao_favorito){
            boolean favorito = mDao.isFavoritos(mHeroi);
            if (favorito){
                mDao.excluir(mHeroi);
            }else{
                mDao.inserir(mHeroi);
            }
            atualizarMenu(!favorito);
            Bus bus = ((HeroiApp)getActivity().getApplication()).getBus();
            bus.post(mHeroi);

        }
        return super.onOptionsItemSelected(item);
    }
}
