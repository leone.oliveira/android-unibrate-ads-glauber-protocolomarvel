package com.example.leone.protocolomarvel;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.leone.protocolomarvel.model.Heroi;

public class HeroiActivity extends AppCompatActivity implements AoClicarNoHeroi {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heroi);

        ViewPager mViewPager = (ViewPager)findViewById(R.id.viewPager);
        mViewPager.setAdapter(new PaginasAdapter(getSupportFragmentManager()));

        TabLayout mTabLayout = (TabLayout)findViewById(R.id.tabs);
        mTabLayout.setTabMode(mTabLayout.MODE_FIXED);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void clicouNoHeroi(Heroi heroi) {
        if (getResources().getBoolean(R.bool.fone)) {
            Intent it = new Intent(this, DetalheHeroiActivity.class);
            it.putExtra("heroi", heroi);
            startActivity(it);
        } else {
            DetalheHeroiFragment dhf = DetalheHeroiFragment.novaInstancia(heroi);
            getSupportFragmentManager().
                    beginTransaction().
                    replace(R.id.detalhe, dhf).
                    commit();
        }
    }

    private class PaginasAdapter extends FragmentPagerAdapter {


        public PaginasAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0){
                return new ListaHeroiFragment();
            } else {
                return new ListaHeroiFavoritoFragment();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String web = getResources().getString(R.string.web);
            String favoritos = getResources().getString(R.string.favoritos);

            return position == 0 ? web : favoritos ;
        }
    }
}
