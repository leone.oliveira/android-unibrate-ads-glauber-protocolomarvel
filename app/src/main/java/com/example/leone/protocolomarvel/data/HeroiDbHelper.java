package com.example.leone.protocolomarvel.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by leone on 04/11/2015.
 */
public class HeroiDbHelper  extends SQLiteOpenHelper {

        public static final String NOME_BANCO = "heroi_db";
        public static final int VERSAO = 1;

        public static final String TABELA_HEROI = "heroi";
        public static final String ID           = "_id";
        public static final String NOME         = "nome";
        public static final String ANO_APARICAO = "ano_aparicao";
        public static final String NACIONALIDADE= "nacionalidade";
        public static final String RACA         = "raca";
        public static final String STATUS       = "status";
        public static final String IMG          = "img";


        public HeroiDbHelper(Context ctx) {
                super(ctx, NOME_BANCO , null, VERSAO);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE "+ TABELA_HEROI +" ( ID INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
                    "                         "+NOME+"          TEXT NOT NULL , \n" +
                    "                         "+ANO_APARICAO+"  INTEGER NOT NULL , \n" +
                    "                         "+RACA+"          TEXT NOT NULL ,\n" +
                    "                         "+NACIONALIDADE+" TEXT NOT NULL , \n" +
                    "                         "+STATUS+"        TEXT NOT NULL , \n" +
                    "                         "+IMG+"           TEXT NOT NULL ) " );
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

}
