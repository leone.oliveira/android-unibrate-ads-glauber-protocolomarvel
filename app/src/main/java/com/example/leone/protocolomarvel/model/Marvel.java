package com.example.leone.protocolomarvel.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Marvel {
    @SerializedName("Protocolo Marvel")
    public List<Categoria> categorias;
}
