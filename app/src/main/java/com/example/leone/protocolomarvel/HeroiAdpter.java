package com.example.leone.protocolomarvel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.leone.protocolomarvel.model.Heroi;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by leone on 25/10/2015.
 */
public class HeroiAdpter extends ArrayAdapter<Heroi>{

    public HeroiAdpter(Context context, List<Heroi> heroi) {
        super(context, 0, heroi);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Heroi heroi = getItem(position);

        ViewHolder holder;

        if (convertView ==null){
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.item_heroi, null);
            holder = new ViewHolder();
            holder.imgCapa = (ImageView)convertView.findViewById(R.id.imgCapa);
            holder.txtNome = (TextView)convertView.findViewById(R.id.txtNome);
            holder.txtAno = (TextView)convertView.findViewById((R.id.txtAno));

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }

        Picasso.with(getContext())
                .load(heroi.img)
                .into(holder.imgCapa);
        holder.txtNome.setText(heroi.nome);
        holder.txtAno.setText(String.valueOf(heroi.ano_aparicao));


        return convertView;
    }

    class ViewHolder{
        ImageView imgCapa;
        TextView txtNome;
        TextView txtAno;
    }
}
