package com.example.leone.protocolomarvel.model;

import java.io.Serializable;

public class Heroi implements Serializable{

    public String nome;
    public int ano_aparicao;
    public String nacionalidade;
    public String raca;
    public String status;
    public String img;

    public Heroi(String nome, int ano_aparicao, String nacionalidade, String raca, String status, String img) {
        this.nome = nome;
        this.ano_aparicao = ano_aparicao;
        this.nacionalidade = nacionalidade;
        this.raca = raca;
        this.status = status;
        this.img = img;
    }

    @Override
    public String toString() {
        return nome + " - " + ano_aparicao;
    }
}
