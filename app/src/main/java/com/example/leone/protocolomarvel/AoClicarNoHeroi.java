package com.example.leone.protocolomarvel;

import com.example.leone.protocolomarvel.model.Heroi;

/**
 * Created by leone on 05/11/2015.
 */
public interface AoClicarNoHeroi {

    void clicouNoHeroi(Heroi heroi);
}
